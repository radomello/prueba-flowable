package com.kairosds.flowable;

import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

@Service
public class DenyTask implements JavaDelegate {
  @Override
  public void execute(final DelegateExecution delegateExecution) {
    System.out.println("Ha denegado");
  }
}
