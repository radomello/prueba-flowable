package com.kairosds.flowable;

import java.util.HashMap;

import java.util.Map;
import org.flowable.bpmn.model.Process;
import org.flowable.bpmn.model.UserTask;
import org.flowable.engine.FormService;
import org.flowable.engine.ManagementService;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.form.TaskFormData;
import org.flowable.engine.impl.persistence.entity.ExecutionEntityImpl;
import org.flowable.engine.runtime.Execution;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PruebaController {

  @Autowired
  private RepositoryService repositoryService;
  @Autowired
  private RuntimeService runtimeService;
  @Autowired
  private TaskService taskService;
  @Autowired
  private FormService formService;

  @GetMapping(value = "/prueba")
  public void prueba() {
    ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("prueba", new HashMap<>());

    Task task =
        taskService.createTaskQuery().taskAssignee("RDA").active().singleResult();

    Process process = repositoryService.getBpmnModel(task.getProcessDefinitionId()).getMainProcess();

    UserTask userTask = (UserTask) process.getFlowElement(task.getTaskDefinitionKey());

    userTask.getOutgoingFlows();

    Map<String, Object> variables = new HashMap<>() {{
      put("action", "approve");
    }};

    taskService.complete(task.getId(), variables);

    String str = "";


  }
}
